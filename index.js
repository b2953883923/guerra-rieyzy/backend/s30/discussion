// [SECTION] Exponent Operator 

const firstNum = 8 ** 2;

console.log("Result of Exponent Operator");
console.log(firstNum);

const thirdNum = 6 ** 3;
console.log(thirdNum);

// Older approach

const secondNum = Math.pow(8,2);
console.log(secondNum);

// [SECTION] Template Literals

/*
	- Allows us to write strings without using the concatenation operator (+)
	- Greatly helps with code readabilty.

*/ 

let name = "John";

// Pre-Template Literat String (Concatation)
// Using single quote 

let message = 'Hello ' + name + '! Welcome to programming';
console.log("Message without template literals: " + message);

// String Using Template Literals
// Uses backticks (``)

message = `Hello ${name}! Welcome to programming!`; 
console.log(`Message with template literals: ${message}` );

// Multi-line Using Template Literals
const anotherMessage = `
${name} attended a math competition.
He won by solving the programing 8 raise to 2 with the solution of ${firstNum}.
`;
console.log(anotherMessage);

/*
	- Template Literals allow us to write strings with embedded JavaScript expression.
	- expression are any valid unit of code that resolves to a value.
	- "${}" are used to include JavaScript expressions in string using template literals.

*/ 

const interestRate = .1;
const principal = 1000;

console.log(`the interest on your saving account is ${principal * interestRate}`);

// [SECTION] Array Destructuring

/*
		- Allows us to unpack the elements in an arraya into distinct variables
		- Allows us to name these array elements with variable instead of using index number.
		- This is done to help in code readability.

		Syntax: 

		let/const [variableNameA, variableNameB, variableNameC] = arrayName;

*/

let fullName = ["Juan", "Dela","Cruz"];
// Pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! its nice to see you again` );

// Array Destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! Its nice to see you again` );


// [SECTION] Object Desctructuring

 /*
	- Allows us to unpack properties of objects into disctinct variable.

	Syntax:
		let/const {propertyNameA, propertyNameB, propertyNameC} = objectName;

 */ 

 const person = {
 	givenName : "Jane",
 	maidenName: "dela",
 	familyName: "Cruz"
 }

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! Its nice to see you again` );


// OBject Destructuring

// const {givenName, maidenName, familyName} = person;
// console.log(givenName);
// console.log(maidenName);
// console.log(familyName);
// console.log(`Hello ${givenName} ${maidenName} ${familyName}! Its nice to see you again` );


// This approach is often used in prop drilling in ReactJS.
function getFullName(givenName, maidenName, familyName) {
	console.log(`Hi! My name is ${givenName} ${maidenName} ${familyName}.`);
}

getFullName(person);

// [SECTION] Arrow Functions
/*
	- Compact alternative syntax to the traditional functions.
	
	Syntax:
		let/const variableName = () => {
			statement/code block
		}
*/ 

const hello = () => {
	console.log("Hello world");
}

hello();

// Pre-Arrow Function and Pre-template Literals.

// function printFullName(firstName, middleInitial, lastName){
// 	console.log(firstName + ' ' + middleInitial + ' ' + lastName);
// }
// printFullName("John", "D", "Smith");

const printFullName = (firstName, middleName, lastName) => {
	console.log(`Hi! My name is ${firstName} ${middleName} ${lastName}.`);
}

printFullName("John", "D", "Smith");

const students = ["John", "Jane", "Judy"];
// Arrow function with loop

	// Pre-Arrow Function
		students.forEach(function(student){
			console.log(students + "is my student.")
		})

	// Arrow Function
	students.forEach((student) => {
		console.log(`${student} is my student`)
	})

// [SECTION] Implicit Return Statement

/*
	- There are instances when you can omit the "return" statement.
	- This works because even without the "return" statement Javascript implicity persforms the operation inside the function.
*/ 

// Pre-arrow Function
// const add = (x ,y ) => {
// 	return x + y;
// }

// let total = add(3, 6);
// console.log(total);

// Arrow function
const add = (x , y ) => x + y;

let total = add(3, 6);
console.log(total);

// [SECTION] Default Function Argument Value
	// Provides a default argument value if none is provided when the function is invoked.

const greet = (name = "User") => `Good morning, ${name}!`;

console.log(greet());
console.log(greet("Jordan"));

// [SECTION] CLass-Based Object Blueprints

/*
	Syntax:
	class className {
		constructor(objectPropertyA, objectPropertyB){
			this.objectPropertyA = objectPropertyA,
			this.objectPropertyB = objectPropertyB
		}
	}
	
*/ 

class Car {
	constructor(brand, name , year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

let myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Raptor Ranger";
myCar.year = "2022"

console.log(myCar);


//We instantiated and initialized out new car.
const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);